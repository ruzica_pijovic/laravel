const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
  context: __dirname + "/src",
  entry: "./app.js",
  output: {
    filename: "app.js",
    path: __dirname + "/build",
  },
  // devServer: {
  //   historyApiFallback: true,
  //   // contentBase: '/',
  //   hot: true,
  //   // compress: true,
  //   // inline: false, // Change to false for IE10 dev mode
  // },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/react', '@babel/env']
          },
        }
      },

      // Typescript loader
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: ['awesome-typescript-loader'],
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
          'sass-loader',
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json']
  },
  plugins: [
    // Re-generate index.html with injected script tag.
    // The injected script tag contains a src value of the
    // filename output defined above.
    new HtmlWebPackPlugin({
      inject: true,
      template: './index.html',
    }),
  ],
}