import 'babel-polyfill';
import './css/app.scss';

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './views/App';

class Client extends Component {
    render () {
        return (
          <BrowserRouter>
              <App />
          </BrowserRouter>
        )
    }
}

export default Client;

if (document.getElementById('app')) {
    ReactDOM.render(<Client />, document.getElementById('app'));
}
