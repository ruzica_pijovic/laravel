import React, {Component} from 'react'
import { Route, Switch, withRouter } from 'react-router-dom';
import Home from './components/Home';
import NotFound from './views/NotFound'
import Game from './views/Game';
import Header from './components/Header';

class Main extends Component {
    constructor(props) {
        super(props);
    }

    getRoutes = () => (
      <Switch>
          <Route exact path={ '/game/:slug' } component={ Game } />
          <Route exact path='/' component={ Home } />
          {/*Page Not Found*/}
          <Route component={ NotFound }/>
      </Switch>
    );

    render() {
        return (
          <div>
              <Header />
              { this.getRoutes() }
          </div>
        )
    }
}

export default withRouter(Main);