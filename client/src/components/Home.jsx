import React, {Component} from 'react'
import GameLIstContainer from './GameLIstContainer';

class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <span>
          This will have list of all available games
        </span>
        <GameLIstContainer/>
      </div>
    )
  }
}

export default Home