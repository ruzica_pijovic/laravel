import React, { Component, Fragment } from 'react'
import {Link, withRouter} from 'react-router-dom';

class Header extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <ul className="nav ">
            <li><Link className="nav-link nav-tabs" to="/">Home</Link></li>
          </ul>
        </div>
      </nav>
    )
  }
}
export default withRouter(Header)