import React from 'react';
import * as PropTypes from 'prop-types';

const Score = (props) => {
  const {
    hit,
    miss,
    left
  } = props;

  return (
    <div className='scores'>
      <ul className="navbar nav navbar-brand">
        <li>Score</li>
        <li className='green'>Hit: { hit }</li>
        <li className='red'>Miss: { miss }</li>
        <li className='blue'>Left: { left }</li>
      </ul>
    </div>
  );
};

Score.propTypes = {
  hit: PropTypes.number,
  miss: PropTypes.number,
  left: PropTypes.number,
};

export default Score;
