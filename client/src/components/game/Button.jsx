import React from 'react';
import * as PropTypes from 'prop-types';

const Button = (props) => {
  const {
    text,
    className,
    callBack
  } = props;

  return (
    <button className={ className } onClick={ callBack }>{ text }</button>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  callBack: PropTypes.func,
};

Button.defaultProps = {
  text: 'Ok',
  className: 'button',
};

export default Button;
