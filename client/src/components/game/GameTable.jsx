import React from 'react';
import * as PropTypes from 'prop-types';

const GameTable = (props) => {
  const {
    letterMappings,
  } = props;

  return (
    <div className='container'>
      <table className="table table-borderless">
        <tbody>
        { letterMappings.map((row, index) => {
          return (
            <tr key={ `row-${ index }` }>
              { GameTable.renderTableCells(row) }
            </tr>
          );
        })
        }
        </tbody>
      </table>
    </div>
  );
};

GameTable.renderTableCells = (row) => {
  return Object.keys(row).map(number => {
    return <td key={ number } className={ `${ row[number].color }` } >{ `${ row[number].value } (${ number })` }</td>
  })
}

GameTable.propTypes = {
  letterMappings: PropTypes.array,
};

export default GameTable;
