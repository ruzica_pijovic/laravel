import React, {Component} from 'react'
import { Link } from 'react-router-dom';

class GameLIstContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      games: [],
    }

    this.getData();
  }
  getData = () => {
    axios.get(`http://localhost:14090/api/v1/games`)
      .then(res => {
        const games = res.data.games;
        this.setState({ games });
      })
  }

  render() {
    return (
      <div>
        <ul>
          { this.state.games.map(game => <li key={ game.slug } className="has-sub"><Link to={ `/game/${ game.slug }` }>{ game.name }</Link></li>)}
        </ul>
      </div>
    )
  }
}

export default GameLIstContainer