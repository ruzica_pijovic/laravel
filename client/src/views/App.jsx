import React, { Component } from 'react'
import { withRouter, Route, Switch } from 'react-router-dom'
import Main from '../Router';

class App extends Component {
    render () {
        const defaultPath = '/:page*';

        return (
          <Switch>
            <Route path={ defaultPath } render={ () => <Main /> }/>
          </Switch>
        )
    }
}

export default withRouter(App);
