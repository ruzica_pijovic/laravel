import React, {Component} from 'react'
import { Link, Redirect, withRouter } from 'react-router-dom';
import * as PropTypes from 'prop-types';
import Button from '../components/game/Button';
import Score from '../components/game/Score';
import GameTable from '../components/game/GameTable';

class Game extends Component {
  static propTypes = {
    match: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      activeLevel: 3000,
      settings: [],
      gameData: [],
      isGameActive: false,
      randomNumber: null,
      counter: 0,
      maxHits: 26,
      userHit: 0,
      userMiss: 0,
      usedNumbers: [],
      disableInput: true,
      letterMappings: [
        {
          1: { value: "a", color: "grey"},
          2: { value: "b", color: "grey"},
          3: { value: "c", color: "grey"},
          4: { value: "d", color: "grey"},
          5: { value: "e", color: "grey"},
          6: { value: "f", color: "grey"},
        },
        {
          7: { value: "g", color: "grey"},
          8: { value: "h", color: "grey"},
          9: { value: "i", color: "grey"},
          10: { value: "j", color: "grey"},
          11: { value: "k", color: "grey"},
          12: { value: "l", color: "grey"},
        },
        {
          13: { value: "m", color: "grey"},
          14: { value: "n", color: "grey"},
          15: { value: "o", color: "grey"},
          16: { value: "p", color: "grey"},
          17: { value: "q", color: "grey"},
          18: { value: "r", color: "grey"},
        },
        {
          19: { value: "s", color: "grey"},
          20: { value: "t", color: "grey"},
          21: { value: "u", color: "grey"},
          22: { value: "v", color: "grey"},
          23: { value: "w", color: "grey"},
          24: { value: "x", color: "grey"},
        },
        {
          25: { value: "y", color: "grey"},
          26: { value: "z", color: "grey"},
        },
      ],
    }
    this.inputFocus = this.utilizeFocus();

    this.getData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { disableInput } = this.state;
    if (prevState.disableInput && !disableInput) {
      this.inputFocus.setFocus();
    }
  }

  utilizeFocus = () => {
    const ref = React.createRef()
    const setFocus = () => {ref.current &&  ref.current.focus()}

    return {setFocus, ref}
  }
  getData = () => {
    const { match } = this.props;
    console.log(match.params.slug)
    if (!match.params.slug) {
      // return <Redirect to={ '/' } />;
    }
    axios.get(`http://localhost:14090/api/v1/game/${ match.params.slug }`)
      .then(res => {
        const settings = res.data.levels;
        const gameData = res.data.game;
        this.setState({
          settings,
          gameData,
        });
      })
  };

  handleUserInput = (e) => {
    const { isGameActive } = this.state

    if (!isGameActive) {
      return;
    }

    this.checkUserInput(e.key);
    this.setState({
      disableInput: true,
    });
  }

  checkUserInput = (value) => {
    const { userHit, userMiss, letterMappings, randomNumber } = this.state

    let isHit = false;
    letterMappings.forEach((row) => {
      if (row[randomNumber]) {
        isHit = value === row[randomNumber].value;

        row[randomNumber].color = isHit ? 'green' : "red";
      }
    });

    let missed = userMiss;
    let hit = userHit;
    if (isHit) {
      hit += 1;
    }
    if (!isHit) {
      missed += 1;
    }

    this.setState({
      userMiss: missed,
      userHit: hit,
    });
  }

  resetTableColors = () => {
    const { letterMappings } = this.state

    letterMappings.forEach((row) => {
      Object.keys(row).forEach(number => {
        row[number].color = 'grey';
      })
    });
  }

  startGame = () => {
    const { isGameActive } = this.state;
    if (isGameActive) {
      this.restartCounterAndSaveGame();

      return;
    }

    this.setState({
      isGameActive: !isGameActive,
      disableInput: false,
      userMiss: 0,
      userHit: 0,
      usedNumbers: [],
    }, () => {
      this.generateRandomNumber();
      this.startTimer();
      this.inputFocus.setFocus()
    });
  }

  startTimer = () => {
    const { activeLevel } = this.state;

    this.timer = setInterval(() => this.tick(), activeLevel);
  };

  tick = () => {
    const { maxHits, userHit, userMiss, usedNumbers } = this.state

    if (maxHits - (userHit + userMiss) === 0) {
      this.restartCounterAndSaveGame();
    }

    if ((userHit + userMiss) < usedNumbers.length) {
      this.checkUserInput('');
    }

    this.generateRandomNumber();
  }

  generateRandomNumber = () => {
    const { usedNumbers } = this.state

    const min = 1;
    const max = 27;
    const rand = Math.floor(min + Math.random() * (max - min));

    if (usedNumbers.length === 26) {
      return ;
    }

    if (usedNumbers.includes(rand)) {
      return this.generateRandomNumber();
    }

    this.setState({
      randomNumber: rand,
      usedNumbers: [...usedNumbers, rand],
      disableInput: false,
    });
  }

  restartCounterAndSaveGame = () => {
    const { isGameActive } = this.state;

    clearInterval(this.timer);
    this.resetTableColors();

    this.setState({
      counter: 0,
      randomNumber: '',
      isGameActive: !isGameActive,
    });

    //todo save data
  }
  onLevelChanged = (timeOut) => {
    this.setState({
      activeLevel: timeOut,
    });
  }

  render() {
    const {
      gameData,
      letterMappings,
      isGameActive,
      randomNumber,
      userHit,
      userMiss,
      maxHits,
      disableInput,
      settings,
      activeLevel,
    } = this.state

    return (
      <div>
        <h1 className='game-headline'> { gameData['name'] }</h1>
        <div className='flex'>
          <div className="container col-md-4">
            <ul className="navbar nav">
              { settings.map(setting => {
                return (
                  <li key={ setting.timeout }>
                    <input
                      type="radio"
                      name="site_name"
                      className='m-2'
                      value={ setting.timeout }
                      disabled={ isGameActive }
                      checked={ activeLevel === setting.timeout }
                      onChange={ () => this.onLevelChanged(setting.timeout) }
                    />
                    { setting.name }
                  </li>
                )
              })}
            </ul>
            <div className="centred-container">
              <Button className="button blue" callBack={ this.startGame } text={ isGameActive? 'Stop': 'Start' } />
            </div>
            <div className="centred-container">
              { randomNumber }
            </div>
            <div className="centred-container">
              <input
                type='text'
                disabled={ disableInput }
                onKeyPress={ this.handleUserInput }
                ref={ this.inputFocus.ref }
                autoFocus
              />
            </div>
          </div>
          <Score hit={ userHit } miss={ userMiss } left={ maxHits - (userHit + userMiss) } />
        </div>
        <GameTable letterMappings={ letterMappings } />
      </div>
    )
  }
}
export default withRouter(Game)
