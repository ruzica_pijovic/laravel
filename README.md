# README #

## Summery #
Project is separated in 2 parts: client (react) and backend (laravel).
#
Admin app contains Login, List of games, Create/Update game and Add Levels for the game.
Admin URL: http://localhost:14090/

#
Client app has List od available games and Game interface.
Client URL: http://localhost:14020/

* Bugs and TODOs:
    * Saving the scores is a feature which is not yet implemented.
    * Fix Game page refresh issue

### Docker guide
  * Setup docker and docker-compose on your environment
    * https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04 (or https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce)
    * https://docs.docker.com/compose/install/
  * You will need the following ports open:
    * 14020 (ReactJS client)    
    * 14090 (nginx)
    * 14092 (db)
    * 9001 (xdebug)

## Client up and running
    * cd client/
    * npm install
    * npm start

## Laravel up and running
    * cd laravel-app/
    * cp env.example .env
    * composer install 
    * docker-compose up -d
    * docker exec app php artisan migrate:refresh --seed
 

## Laravel Open Api documentation 
    * http://localhost:14090/api/documentation