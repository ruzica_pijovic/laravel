<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Game extends Model
{
    use Sluggable;

    const STATUS_ACTIVE = 1;

    protected $fillable = [
        'name',
        'slug',
        'status',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get the levels record associated with the user.
     */
    public function levels()
    {
        return $this->hasMany('App\Level');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}