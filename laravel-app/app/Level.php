<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $fillable = [
        'name',
        'timeout',
        'status',
        'game_id',
    ];

    /**
     * Get the game that owns the level.
     */
    public function game()
    {
        return $this->belongsTo('App\Game');
    }
}