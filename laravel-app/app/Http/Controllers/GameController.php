<?php

namespace App\Http\Controllers;

use App\Game;
use App\Level;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $games = Game::all();

        return view('home', compact('games'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('games.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
        ]);

        $game = new Game([
            'name' => $request->get('name'),
            'status' => Game::STATUS_ACTIVE,
        ]);

        $game->save();

        return redirect('/')->with('success', 'Game saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int     $gameId
     *
     * @return RedirectResponse
     */
    public function storeLevel(Request $request, int $gameId)
    {
        $game = Game::find($gameId);

        $request->validate([
            'name'=>'required',
            'timeout'=>'required|numeric',
        ]);

        $level = new Level([
            'name' => $request->get('name'),
            'timeout' => $request->get('timeout'),
            'game_id' => $gameId,
            'status' => Game::STATUS_ACTIVE,
        ]);

        $level->save();

        return Redirect::route('games.edit', [$game->slug])->with('success', 'Level saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Game $game
     *
     * @return Application|Factory|View
     */
    public function edit(Game $game)
    {
        return view('games.edit', ['game' => $game, 'levels' => $game->levels()->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Game    $game
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function update(Request $request, Game $game)
    {
        $request->validate([
            'name'=>'required',
        ]);

        $game->name = $request->get('name');
        $game->save();

        return redirect('/')->with('success', 'Game updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Game $game
     *
     * @return Application|RedirectResponse|Redirector
     *
     * @throws \Exception
     */
    public function destroy(Game $game)
    {
        $game->delete();

        return redirect('/')->with('success', 'Game deleted!');
    }
}
