<?php

namespace App\Http\Controllers\Api\V1;

use App\Game;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    /**
     *
     * @OA\Get(
     *      path="/games",
     *      operationId="index",
     *      tags={"Game"},
     *      summary="Get list of games",
     *      description="Returns list of games",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index()
    {
        $data['games'] = DB::table('games')->where('status', 1)->orderBy('id')->get();
//        $data['levels'] = Game::where('slug', 'test')->first()->levels;

        return new JsonResponse($data);
    }


    /**
     *
     * @OA\Get(
     *      path="/game/{game}",
     *      operationId="getGamaData",
     *      tags={"Game"},
     *      summary="Get game data",
     *      description="Returns ame data",
     *      @OA\Parameter(
     *          name="game",
     *          description="Game slug",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     * @param Game $game
     *
     * @return JsonResponse
     */
    public function getGamaData(Game $game)
    {
        return new JsonResponse(['levels' => $game->levels()->get(), 'game' => $game]);
    }
}