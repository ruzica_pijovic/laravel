<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
//    /**
//     * Create user
//     *
//     * @param Request $request
//     *
//     * @return \Illuminate\Http\JsonResponse [string] message
//     */
//    public function signup(Request $request)
//    {
//        $request->validate([
//            'name' => 'required|string',
//            'email' => 'required|string|email|unique:users',
//            'password' => 'required|string|confirmed'
//        ]);
//        $user = new User([
//            'name' => $request->name,
//            'email' => $request->email,
//            'password' => bcrypt($request->password)
//        ]);
//        $user->save();
//        return response()->json([
//            'message' => 'Successfully created user!'
//        ], 201);
//    }

    /**
     * Login user and create token
     *
     * @param Request $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials)) {
            return redirect('login');
        }

        return redirect()->intended('/');
    }

    /**
     * Logout user
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function logout()
    {
        Auth::logout();

        return redirect('login');
    }
}