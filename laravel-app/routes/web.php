<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/', 'GameController@index');

    Route::group([
        'prefix' => ''
    ], function () {
//        Route::post('signup', 'AuthController@signup');
        Route::post('login', [ 'as' => 'login', 'uses' => 'AuthController@login']);
        Route::get('login', function () { return view('auth/login');});

        Route::group([
            'middleware' => 'auth'
        ], function() {
            Route::post('logout', [ 'as' => 'logout', 'uses' => 'AuthController@logout']);
            Route::get('user', 'AuthController@user');
            Route::get('/game/create', [ 'as' => 'games.create', 'uses' => 'GameController@create']);
            Route::post('/game/store', [ 'as' => 'games.store', 'uses' => 'GameController@store']);
            Route::get('/game/{game}/edit', [ 'as' => 'games.edit', 'uses' => 'GameController@edit']);
            Route::patch('/game/{game}/update', [ 'as' => 'games.update', 'uses' => 'GameController@update']);
            Route::delete('/game/{game}/delete', [ 'as' => 'games.destroy', 'uses' => 'GameController@destroy']);
            Route::post('/game/{id}/level/store', [ 'as' => 'games.levels.store', 'uses' => 'GameController@storeLevel']);
        });
    });


//Route::view('/{path?}', 'app');
