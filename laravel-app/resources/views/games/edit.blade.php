@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1>Add new Game</h1>
        <div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('games.update', $game->slug) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value={{ $game->name }}/>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
            <br/>
            <h1>Game Levels</h1>
            <ul>
            @foreach ($levels->all() as $level)
                <li>{{ $level->name }} ( Time Out : {{ $level->timeout }}ms )</li>
            @endforeach
            </ul>
            <br/>
            <h1>Add new Level for this game</h1>
            <form method="post" action="{{ route('games.levels.store', $game->id) }}">
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name"/>
                </div>
                <div class="form-group">
                    <label for="name">Timeout (ms):</label>
                    <input type="number" class="form-control" name="timeout"/>
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
</div>
@endsection
