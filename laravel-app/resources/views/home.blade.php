@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    @endif
                    @guest
                        Please log in!
                    @else
                    <div>
                        <a style="margin: 19px;" href="{{ route('games.create')}}" class="btn btn-primary">New game</a>
                    </div>
                    @foreach($games as $game)
                    <div style="display: flex;justify-content: space-between; border-bottom: 1px solid lightgray">
                        <div>
                            <div>Name: {{$game->name}}</div>
                            <div>Status: {{$game->status ? "Active" : "Disabled"}}</div>
                        </div>
                        <div style="display: flex; justify-content: flex-end;">
                            <ul>
                                <li style="list-style: none">
                                    <a href="{{ route('games.edit',$game->slug)}}" class="btn btn-primary">Edit</a>
                                </li>
                                <li style="list-style: none">
                                    <form action="{{ route('games.destroy', $game->slug)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @endforeach

                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
