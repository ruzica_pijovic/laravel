<?php

use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            'name' => 'My First Game',
            'slug' => 'my-first-game',
            'status' => 1,
        ]);

        $gameIds = DB::table('games')->first('id');
        var_dump($gameIds->id);

        DB::table('levels')->insert([
            'name' => 'Easy',
            'timeout' => 5000,
            'game_id' => $gameIds->id,
            'status' => 1,
        ]);
        DB::table('levels')->insert([
            'name' => 'Medium',
            'timeout' => 2500,
            'game_id' => $gameIds->id,
            'status' => 1,
        ]);
        DB::table('levels')->insert([
            'name' => 'Hard',
            'timeout' => 2500,
            'game_id' => $gameIds->id,
            'status' => 1,
        ]);
    }
}
